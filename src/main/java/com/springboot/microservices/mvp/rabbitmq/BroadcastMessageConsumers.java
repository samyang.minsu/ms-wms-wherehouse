package com.springboot.microservices.mvp.rabbitmq;

import org.apache.ibatis.jdbc.Null;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.springboot.microservices.mvp.model.Stock;

import com.springboot.microservices.mvp.dao.LocDao;
import com.springboot.microservices.mvp.model.Loc;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class BroadcastMessageConsumers {
	
	@Autowired
	private LocDao locDao;
	
	//@Autowired
	//private BroadcastMessageProducer broadcastMessageProducer;
	
	
	@RabbitListener(queues = "${prop.rabbit.queue.input}" )
	public void receiveMessageFromDirectInputWithQueue(Stock stock) {
		Loc loc = new Loc();
		
		try
		{
			log.debug("WMS_ITEM_INPUT Receive : "+stock.toString());
			
			//창고 가용 상태 업데이트
			loc.setLocCd(stock.getLocCd());
			loc.setUseYn(stock.getUseYn());
			
			if(stock.getUseYn().equals("N")) 
			{	
				// 비가용으로 업데이트
				loc.setBarcode(stock.getBarcode());
				loc.setOnhandQty(100);
			}
			else
			{
				// 가용으로 업데이트
				loc.setBarcode("");  
				loc.setOnhandQty(0);
			}
			
			log.info("Start db update");
			int re  = locDao.updateLoc(loc);
			log.debug("result :"+ re);
			
			/*
			stock.setQty(0);
			
			//성공이면 qty=1리턴
			if(re == 1)
			{
				stock.setQty(1);
				// rabbitmq
				broadcastMessageProducer.produceWmsItemInputRe(stock);
			}
			else
			{
				// rabbitmq
				broadcastMessageProducer.produceWmsItemInputRe(stock);
			}
			*/
		}
		catch(Exception ex)
		{
			//실패면 '0'리턴
			//broadcastMessageProducer.produceWmsItemInputRe(stock);
		}
	}
	
}
