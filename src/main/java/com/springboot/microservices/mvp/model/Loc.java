package com.springboot.microservices.mvp.model;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class Loc {
	 private String  locCd;	 
	 private String  locNm; 
	 private int 	 onhandQty;  
	 private int  	 capaQty;  
	 private String  useYn;
	 private String	 barcode;
	 
	 private BigDecimal avPer;
	 private BigDecimal usePer;
	 
	 private int totalQty;
	 private int useQty;
	 private int unUseQty;
}
